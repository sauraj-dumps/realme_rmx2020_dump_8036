## sys_oplus_mssi_64_cn-user 11 RP1A.200720.011 1607914664672 release-keys
- Manufacturer: realme
- Platform: mt6768
- Codename: RMX2020
- Brand: realme
- Flavor: lineage_RMX2020-eng
- Release Version: 11
- Id: RQ3A.211001.001
- Incremental: eng.neolit.20211102.153510
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/coral/coral:12/SP1A.210812.016.A1/7796139:user/release-keys
- OTA version: 
- Branch: sys_oplus_mssi_64_cn-user-11-RP1A.200720.011-1607914664672-release-keys
- Repo: realme_rmx2020_dump_8036


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
